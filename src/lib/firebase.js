import Firebase from 'firebase/app';
import 'firebase/firestore';
import 'firebase/auth';

import {seedDatabase} from '../seed';

const config = {
  apiKey: "AIzaSyAYtB4nzXMlK_QT0d0akIfA6T0suH137H4",
  authDomain: "instagram-clone-66f27.firebaseapp.com",
  projectId: "instagram-clone-66f27",
  storageBucket: "instagram-clone-66f27.appspot.com",
  messagingSenderId: "939901362204",
  appId: "1:939901362204:web:71487c4dd53a78734a7fd9"
};

const firebase = Firebase.initializeApp(config);
const { FieldValue } = Firebase.firestore;

//seedDatabase(firebase);

export { firebase, FieldValue };
